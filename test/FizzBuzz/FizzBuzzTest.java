/**
 *
 */
package FizzBuzz;


import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author nakahara.yu
 *
 */
public class FizzBuzzTest {

	/**
	 * @throws java.lang.Exception
	 */

	/**
	 * @throws java.lang.Exception
	 */



	@Test
	public void test1() {
		//引数に9を渡した場合「Fizz」が取得できる
		assertEquals("Fizz",FizzBuzz.checkFizzBuzz(9));
	}
	
	@Test
	public void test2() {
		//引数に20を渡した場合「Buzz」が取得できる
		assertEquals("Buzz",FizzBuzz.checkFizzBuzz(20));
	}
	
	@Test
	public void test3() {
		//引数に45を渡した場合「FizzBuzz」が取得できる
		assertEquals("FizzBuzz",FizzBuzz.checkFizzBuzz(45));
	}
	
	@Test
	public void test4() {
		//引数に44を渡した場合「44」が取得できる
		assertEquals("44",FizzBuzz.checkFizzBuzz(44));
	}
	
	@Test
	public void test5() {
		//引数に46を渡した場合「46」が取得できる
		assertEquals("46",FizzBuzz.checkFizzBuzz(46));
	}
	@Test
	public void test6() {
		assertEquals("8",FizzBuzz.checkFizzBuzz(8));
	}

}
